/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_c;

/**
 *
 * @author marius
 */
// Unitatea de compilare Chat_C.java

import java.net.*;
import java.io.*;
import java.util.*;

class Chat_C {

    public static void main(String[] sir) throws IOException {
        Scanner sc = new Scanner(System.in);

    System.out.print("Adresa serverului si portul:");
    String adresa = sc.next(); int port = sc.nextInt();
// Numele: 
        System.out.print("Nickname: ");
        String nick = sc.next().trim();

        final DataInputStream dis;
        DataOutputStream dos;

        Socket cs = null;
        try {
            cs = new Socket(adresa, port);
            dos = new DataOutputStream(cs.getOutputStream());
            dos.writeUTF(nick);
        } catch (IOException e) {
            System.out.println("Conexiune esuata!");
            System.exit(1);
        }

        dis = new DataInputStream(cs.getInputStream());
        dos = new DataOutputStream(cs.getOutputStream());

        while (true) {
            String test;
            test = dis.readUTF();
            if (test.equals("acceptat")) {
                break;
            } else {
                System.out.print("Eroare:\tNumele ales este utilizat.\n"
                        + "\tIncercati conectarea cu alt nume.\n");
                System.exit(1);
            }
        }

// Mesaje primite de la Server    
        Thread GetMessages;
        GetMessages = new Thread(new Runnable() {
            @Override
            public void run() {

                boolean primesc = true;

                while (primesc) {
                    String message;
                    try {
                        message = dis.readUTF();
                        if (message.equals("quit")) {
                            primesc = false;
                        } else {
                            System.out.println(message);
                        }
                    } catch (IOException e) {
                        primesc = false;
                    }
                }
            }
        });
        GetMessages.start();

// Mesaje catre Server    
        String linie;
        while (cs.isConnected()) {
            linie = sc.nextLine().trim();
            dos.writeUTF(linie);
           
            if (linie.equals("quit")) {
                dis.close();
                dos.close();
                cs.close();
                GetMessages.stop();
                System.exit(0);
                break;
            }
        }
    }
}
